import { sequelize } from "..";
import { DataTypes } from "sequelize";

export const Order = sequelize.define('order', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    // *hash_id
    order_status_id: DataTypes.INTEGER,
    customer_id: DataTypes.INTEGER,
    customer_email: {
        type: DataTypes.STRING,
        validate: {
            isEmail: true,
            len: [1, 255],
        }
    },
    billing_name: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 128],
        }
    },
    billing_zip: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 16],
        }
    },
    billing_region_code: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 5],
        }
    },
    billing_region_name: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    },
    billing_city: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    },
    billing_address: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 255],
        }
    },
    billing_phone: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 32],
        }
    },
    billing_tax_number: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 32],
        }
    },
    // *payment_type_const
    // *shipping_type_const
    shipping_name: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 128],
        }
    },
    shipping_zip: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 16],
        }
    },
    shipping_region_code: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 5],
        }
    },
    shipping_region_name: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    },
    shipping_city: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    },
    shipping_address: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 255],
        }
    },
    shipping_phone: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 32],
        }
    },
    shipping_point_id: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 32],
        }
    },
    cart_gross_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    cart_net_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    cart_tax_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    shipping_gross_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    shipping_net_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    shipping_tax_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    shipping_tax_rate: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 6],
            max: 9.9999
            // TODO: jobb float validation
        }
    },
    shipping_tax_type: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 32],
        }
    },
    payment_gross_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    payment_net_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    payment_tax_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    payment_tax_rate: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 6],
            max: 9.9999
            // TODO: jobb float validation
        }
    },
    payment_tax_type: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 32],
        }
    },
    total_gross_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    total_net_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    total_tax_value: {
        type: DataTypes.NUMBER,
        validate: {
            isFloat: true,
            len: [1, 16],
            max: 99999999999.9999
            // TODO: jobb float validation
        }
    },
    total_weight_gr: {
        type: DataTypes.INTEGER,
        validate: {
            len: [1, 11],
        }
    },
    note: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 16777215],
        }
    },
    data_sent_to_analytics: {
        type: DataTypes.BOOLEAN,
        validate: {
            isBoolean: true,
        }
    },
    created_at: {
        type: DataTypes.DATE,
        validate: {
            isDate: true,
        }
    },
    updated_at: {
        type: DataTypes.DATE,
        validate: {
            isDate: true,
        }
    },
    customer_accepted_terms_and_conditions: {
        type: DataTypes.BOOLEAN,
        validate: {
            isBoolean: true,
        }
    },
    customer_accepted_privacy_statement: {
        type: DataTypes.BOOLEAN,
        validate: {
            isBoolean: true,
        }
    },
    payment_method_name: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 128],
        }
    },
    payment_method_description: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 16777215],
        }
    },
    shipping_method_name: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 128],
        }
    },
    shipping_method_description: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 16777215],
        }
    },
    payment_is_online: {
        type: DataTypes.BOOLEAN,
        validate: {
            isBoolean: true,
        }
    },
    // *old
    payment_method_name_for_invoice: {
        type: DataTypes.STRING,
        validate: {
            len: [1, 64],
        }
    },
    invoice_number: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    },
    proform_invoice_number: {
        type: DataTypes.STRING,
        validate: {
            len: [0, 64],
        }
    }
});

// TODO: null teszt