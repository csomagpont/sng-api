import { AppResponse } from "./app-response.interface";
import { HttpStatus } from "../enum/http-status.enum";

export class ErrorResponse implements AppResponse {
    success: boolean;
    data: any;
    status: number;

    constructor(message: string, status = HttpStatus.InternalError) {
        this.success = false;
        this.data = message;
        this.status = status;
    }
}