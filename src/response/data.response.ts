import { AppResponse } from "./app-response.interface";
import { HttpStatus } from "../enum/http-status.enum";

export class DataResponse implements AppResponse {
    success: boolean;
    data: any;
    status: number;

    constructor(data: any, status = HttpStatus.OK) {
        this.success = true;
        this.data = data;
        this.status = status;
    }
}