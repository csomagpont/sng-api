export interface AppResponse {
    success: boolean;
    data: any;
    status: number;
}