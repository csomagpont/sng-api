import { appFactory } from ".";
import * as fs from "fs";
import { ResourceControllerBase } from "./controller/base/resource-controller.base";
import { ModelCtor, Model } from "sequelize/types";

const controllers = appFactory.getControllers();

const document: any = {};

/* Header */

document.openapi = '3.0.0';
document.info = {
    title: 'ShopnGo API',
    description: '',
    version: '1.0.0' // TODO: config fájlból?
};
document.servers = [{
    url: 'https://api.shopngo.hu',
    description: ''
}];


/* Paths */ 

const paths: any = {};

for (const controller of controllers) {
    for (const route of controller.routes) {
        let pathString = transformParameterInPath(`/${controller.path}/${route.path}`);
        let path: any = paths[pathString] = paths[pathString] || {};

        /* Path summary and description */
        path[route.method] = {
            summary: route.openapi.summary,
            description: route.openapi.description
        }
        /* Path parameters */
        if (route.openapi.parameters) {
            path[route.method].parameters = route.openapi.parameters;
        }
        /* Path responses */
        path[route.method].responses = route.openapi.responses;
        if (controller instanceof ResourceControllerBase) {
            const resourceName = getResourceNameFromController(controller);
            path[route.method] = replaceResourceName(resourceName, path[route.method]);
        }
    }
}

document.paths = paths;

/* Components */

const components: any = {};
components.schemas = {
    ErrorResponse: createResponseSchema({
        type: 'string'
    }),
    ListQuery: {
        properties: {
            limit: {
                type: 'integer',
            },
            offset: {
                type: 'integer',
            },
            filter: {
                description: 'One or more nested objects with the following structure: [field][operator]: value',
                type: 'object',
            },
            order: {
                description: 'An array consisting of two elements: 0: field, 1: direction',
                type: 'array',
                items: {
                    type: 'string'
                }
            },
            attributes: {
                type: 'array',
                items: {
                    type: 'string'
                }
            },
        }
    }
};

for (const controller of controllers) {
    if (controller instanceof ResourceControllerBase) {
        const resourceName = getResourceNameFromController(controller);
        const resourceSchema = createSchemaFromResourceModel(controller.getModel());
        components.schemas[resourceName] = resourceSchema;
        components.schemas[resourceName + 'Response'] = createResponseSchema({
            $ref: `#/components/schemas/${resourceName}`
        });
        components.schemas[resourceName + 'ListResponse'] = createResponseSchema(createResourceListSchema(resourceName));
        components.schemas['Patch' + resourceName] = createPatchSchemaFromSchema(resourceSchema);
    }
}

components.securitySchemes = {
    ApiKeyAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'X-API-KEY'
    }
};

document.components = components;

/* SECURITY */

document.security = [{
    ApiKeyAuth: []
}];

let data = JSON.stringify(document, null, 2);
fs.writeFileSync('output/openapi-spec.json', data);


/* HELPER FUNCTIONS */

function transformParameterInPath (path: string) {
    while (path.indexOf(':') !== -1) {
        const index1 = path.indexOf(':');
        path = path.substr(0, index1) + '{' + path.substr(index1 + 1);
        const index2 = path.substring(index1).indexOf('/') === -1 ? -1 : index1 + path.substring(index1).indexOf('/');
        if (index2 > -1) {
            path = path.substr(0, index2 - 1) + '}' + path.substr(index2 + 1);
        } else {
            path += '}';
        }
    }
    return path;
}

function createResponseSchema(dataSchema: any) {
    const responseSchema = {
        type: 'object',
        properties: {
            success: {
                type: 'boolean',
            },
            status: {
                type: 'integer',
            },
            data: dataSchema
        }
    };
    return responseSchema;
}

function createResourceListSchema(resourceName: string): object {
    const listSchema = {
        type: 'object',
        properties: {
            items: {
                type: 'array',
                items: {
                    $ref: `#/components/schemas/${resourceName}`
                }
            },
            count: {
                type: 'integer',
            },
            query: {
                $ref: `#/components/schemas/ListQuery`
            }
        }
    };
    // TODO: betenni a többi elemet (pagination, count, stb)
    return listSchema;
}

function createSchemaFromResourceModel(Model: ModelCtor<Model<any, any>>): object {
    const properties: any = {};
    for (const fieldName in Model.rawAttributes) {
        const fieldData = Model.rawAttributes[fieldName];
        properties[fieldName] = convertModelFieldData(fieldData);
    }
    return {
        type: 'object',
        properties
    };
}

function createPatchSchemaFromSchema(schema: object) {
    const patchSchema: any = Object.assign({}, schema);
    patchSchema.required =  ['id'];
    return patchSchema;
}

function convertModelFieldData(fieldData: any) {
    let fieldType = fieldData.type.constructor.key.toLowerCase();
    if (fieldType === 'date') {
        return {
            type: 'string',
            format: 'date-time'
        }
    }
    return {
        type: fieldType
    }
}

function replaceResourceName(resourceName: string, obj: object) {
    let string = JSON.stringify(obj);
    string = string.replace(/#RESOURCE_NAME#/g, resourceName);
    return JSON.parse(string);
}

function getResourceNameFromController(controller: ResourceControllerBase) {
    return controller.getModel().name.charAt(0).toUpperCase() + controller.getModel().name.slice(1);
}