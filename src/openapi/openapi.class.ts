import { OpenApiResponses } from "./openapi.interface";
import { HttpStatus } from "../enum/http-status.enum";

export class OpenApiResponse {
    description: string;
    content: any;

    constructor(description: string, schemaType: SchemaType) {
        this.description = description;
        this.content = {
            "application/json": {
                schema: {
                    $ref: `#/components/schemas/${schemaType}`
                }
            }
        }
    }
}

export enum SchemaType {
    Resource = '#RESOURCE_NAME#Response',
    ResourceList = '#RESOURCE_NAME#ListResponse',
    Error = 'ErrorResponse',
}

export class OpenApiResourceResponses implements OpenApiResponses {
    [HttpStatus.OK]: OpenApiResponse;
    [HttpStatus.BadRequest]: OpenApiResponse;
    [HttpStatus.UnprocessableEntity]: OpenApiResponse;
    [HttpStatus.NotFound]: OpenApiResponse | undefined;
    [HttpStatus.InternalError]: OpenApiResponse;

    constructor(okDescription: string, isList = false) {
        this[HttpStatus.OK] = new OpenApiResponse(
            'A #RESOURCE_NAME# item list', 
            isList ? SchemaType.ResourceList : SchemaType.Resource);
        this[HttpStatus.BadRequest] = new OpenApiResponse('An error with the request data', SchemaType.Error);
        this[HttpStatus.UnprocessableEntity] = new OpenApiResponse('A validation error', SchemaType.Error);
        if (!isList) {
            this[HttpStatus.NotFound] = new OpenApiResponse('Resource #RESOURCE_NAME# could not be found error', SchemaType.Error);
        }
        this[HttpStatus.InternalError] = new OpenApiResponse('An unexpected server error', SchemaType.Error);
    }
}