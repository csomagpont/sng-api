import { HttpStatus } from "../enum/http-status.enum";
import { OpenApiResponse } from "./openapi.class";

export interface OpenApiParameter {
    name: string;
    in: 'path' | 'query' | 'header' | 'cookie';
    required?: boolean;
    description?: string;
    schema: {
        type: string
    } | {
        $ref: string
    }
}

export interface OpenApiResponses {
    [HttpStatus.OK]?: OpenApiResponse,
    [HttpStatus.BadRequest]?: OpenApiResponse,
    [HttpStatus.NotFound]?: OpenApiResponse,
    [HttpStatus.UnprocessableEntity]?: OpenApiResponse,
    [HttpStatus.GatewayTimeout]?: OpenApiResponse,
    default?: OpenApiResponse
}