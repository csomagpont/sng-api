export enum HttpStatus {
    OK = 200,
    BadRequest = 400,
    Unauthorized = 401,
    NotFound = 404,
    UnprocessableEntity = 422,
    TooManyRequests = 429,
    InternalError = 500,
    GatewayTimeout = 504,
}