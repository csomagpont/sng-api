import { Sequelize } from 'sequelize';

export class SequelizeService {
    // TODO: config fájlba mindent
    sequelize: Sequelize;

    constructor() {
        this.sequelize = this.connect();
    }

    connect(): Sequelize {
        return new Sequelize('mysql://root:@localhost:3306/sng', {
            define: {
                freezeTableName: true,
                timestamps: false
            }
        });
    }

    getSequelize(): Sequelize {
        return this.sequelize;
    }

}