import { SequelizeService } from "./service/sequelize.service";
export const sequelize = new SequelizeService().getSequelize();

import { AppFactory } from "./factory/app.factory";
import { unhandledErrorMiddleware } from "./middleware/unhandled-error.middleware";
import { AppInitializerData } from "./factory/interface/app-initializer-data.interface";
import { notFoundMiddleware } from "./middleware/not-found.middleware";
import { OrderController } from "./controller/order.controller";
import { authMiddleware } from "./middleware/auth.middleware";
import { rateLimiterMiddleware } from "./middleware/rate-limiter.middleware";



export const appInitializerData: AppInitializerData = {
    controllers: [
        OrderController
    ],
    middlewares: [authMiddleware, rateLimiterMiddleware],
    errorHandler: unhandledErrorMiddleware,
    notFoundHandler: notFoundMiddleware
}

export const appFactory = new AppFactory(appInitializerData);
const app = appFactory.getServer();