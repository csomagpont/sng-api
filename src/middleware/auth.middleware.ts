import express from "express";
import { ErrorResponse } from "../response/error.response";
import { HttpStatus } from "../enum/http-status.enum";

export function authMiddleware(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const apiKey = req.header('X-API-KEY');
    // TODO: adatbázis beállítani
    if (!apiKey || apiKey !== 'teszt_api_kulcs') {
        new ErrorResponse('The API key invalid or missing', HttpStatus.Unauthorized);
        // TODO: hiba dokumentálni OpenApi-ba
    }
    next();
}