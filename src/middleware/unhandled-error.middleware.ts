import express from "express";
import randomstring from "randomstring";
import { HttpStatus } from "../enum/http-status.enum";
import { ErrorResponse } from "../response/error.response";

export function unhandledErrorMiddleware(error: any, req: express.Request, res: express.Response, next: express.NextFunction): void {
    if (error.status) {
        res.status(error.status).send(error);
    } else {
        // TODO: logging
        const referenceId = `${Math.floor(Date.now() / 1000)}-${randomstring.generate({length: 4, charset: 'numeric'})}`;
        console.error('SNG-Ref: ' + referenceId, error);
        const errorResponse = new ErrorResponse(`There was an unexpected error. Reference ID: ${referenceId}`, HttpStatus.InternalError);
        res.status(errorResponse.status).send(errorResponse);
    }
}