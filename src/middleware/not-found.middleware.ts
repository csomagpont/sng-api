import express from "express";

export function notFoundMiddleware(req: express.Request, res: express.Response, next: express.NextFunction): void {
    res.status(404).send('This endpoint is not found');
}