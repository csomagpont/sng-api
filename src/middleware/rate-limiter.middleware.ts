import express from "express";
import { ErrorResponse } from "../response/error.response";
import { HttpStatus } from "../enum/http-status.enum";
import { RateLimiterMemory } from 'rate-limiter-flexible';

const requestLimit = 180;
const limitDuration = 60;

const rateLimiter = new RateLimiterMemory({
    points: requestLimit,
    duration: limitDuration
});

export async function rateLimiterMiddleware(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const apiKey = req.header('X-API-KEY') || '';
    try {
        const rateLimiterRes = await rateLimiter.consume(apiKey, 1);
        res.set({
            "Retry-After": rateLimiterRes.msBeforeNext / 1000,
            "X-RateLimit-Limit": requestLimit,
            "X-RateLimit-Remaining": rateLimiterRes.remainingPoints,
            "X-RateLimit-Reset": new Date(Date.now() + rateLimiterRes.msBeforeNext)
        })
        next();
    } catch (rateLimiterRej) {
        res.set({
            "Retry-After": rateLimiterRej.msBeforeNext / 1000,
            "X-RateLimit-Limit": requestLimit,
            "X-RateLimit-Remaining": rateLimiterRej.remainingPoints,
            "X-RateLimit-Reset": new Date(Date.now() + rateLimiterRej.msBeforeNext)
        })
        next(new ErrorResponse(`Only ${requestLimit} requests allowed every ${limitDuration} seconds`, HttpStatus.TooManyRequests));
    }
}