import { ControllerBase } from "../../controller/base/controller.base";
import express from "express";

export interface AppInitializerData {
    controllers: ( new() => ControllerBase )[];
    middlewares: express.RequestHandler[];
    errorHandler: express.ErrorRequestHandler,
    notFoundHandler: express.RequestHandler
}