import express from "express";
import * as bodyParser from 'body-parser';
import { ControllerBase } from "../controller/base/controller.base";
import { AppInitializerData } from "./interface/app-initializer-data.interface";
import Controller from "../controller/interface/controller.interface";

export class AppFactory {
    private app: express.Application = express();

    private controllers: Controller[] = [];

    constructor(appInitializerData: AppInitializerData) {
        this.initializeMiddlewares(appInitializerData.middlewares);
        this.initializeControllers(appInitializerData.controllers);
        this.initializeErrorHandler(appInitializerData.errorHandler);
        this.initializeNotFoundHandler(appInitializerData.notFoundHandler);
        this.startServer();
    }

    public getServer(): express.Application {
        return this.app;
    }

    private startServer(): void {
        const port = process.env.PORT || 3000
        this.app.listen(port, () => {
            console.log(`App listening on port ${port}`);
        });
        
    }

    private initializeMiddlewares(middlewares: express.RequestHandler[]): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        for (const middleware of middlewares) {
            this.app.use(middleware);
        }
    }

    private initializeControllers<ControllerT extends ControllerBase>(ControllerTypes: { new(): ControllerT }[]) {
        for (const ControllerType of ControllerTypes) {
            this.addController(ControllerType);
        }
    }

    private addController<ControllerT extends ControllerBase>(ControllerType: { new(): ControllerT }) {
        const controller = new ControllerType();
        this.controllers.push(controller);
        this.app.use(`/${controller.path}`, controller.router)
    }

    public getControllers(): Controller[] {
        return this.controllers;
    }

    private initializeErrorHandler(errorHandler: express.ErrorRequestHandler) {
        this.app.use(errorHandler);
    }

    private initializeNotFoundHandler(notFoundHandler: express.RequestHandler) {
        this.app.use(notFoundHandler);
    }
}