import { ControllerBase } from "./controller.base";
import * as express from 'express';
import { ErrorResponse } from "../../response/error.response";
import { HttpMethod } from "../../enum/http-method.enum";
import { HttpStatus } from "../../enum/http-status.enum";
import { ModelCtor, Model, Op } from "sequelize";
import { FindOptions } from "sequelize";
import { Route } from "../interface/route.interface";
import { OpenApiResourceResponses } from "../../openapi/openapi.class";

export abstract class ResourceControllerBase extends ControllerBase {

    public Model!: ModelCtor<Model<any, any>>;

    public routes: Route[] = [
        {
            path: '', 
            method: HttpMethod.Get,
            handler: this.list,
            openapi: {
                summary: 'Get list of #RESOURCE_NAME# items',
                responses: new OpenApiResourceResponses('A #RESOURCE_NAME# list', true),
                parameters: [
                    {
                        name: 'limit',
                        in: 'query',
                        schema: {
                            type: 'integer'
                        }
                        // TODO: max limit, default?
                    }, {
                        name: 'offset',
                        in: 'query',
                        schema: {
                            type: 'integer'
                        },
                    }, {
                        name: 'order',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        description: 'Format: {field},{direction}'
                    }, {
                        name: 'attributes',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        description: 'Format: {attribute1},{attribute2},...'
                    }
                    // TODO: filter
                ],
            }
        },
        {
            path: ':id', 
            method: HttpMethod.Get,
            handler: this.get,
            openapi: {
                summary: 'Get single #RESOURCE_NAME# resource',
                parameters: [{
                    name: 'id',
                    in: 'path',
                    required: true,
                    schema: {
                        type: 'integer'
                    }
                }],
                responses: new OpenApiResourceResponses('A #RESOURCE_NAME# item')
            }
        },
        {
            path: ':id', 
            method: HttpMethod.Patch,
            handler: this.patch,
            openapi: {
                summary: 'Update #RESOURCE_NAME# resource',
                parameters: [
                    {
                        name: 'id',
                        in: 'path',
                        required: true,
                        schema: {
                            type: 'integer'
                        }
                    }, {
                        name: '#RESOURCE_NAME#',
                        in: 'path',
                        required: true,
                        schema: {
                            $ref: `#/components/schemas/Patch#RESOURCE_NAME#`
                        }
                    }
                ],
                responses: new OpenApiResourceResponses('The modified #RESOURCE_NAME# item'),
            }
        },
        {
            path: ':id', 
            method: HttpMethod.Delete,
            handler: this.delete,
            openapi: {
                summary: 'Delete #RESOURCE_NAME# item',
                parameters: [{
                    name: 'id',
                    in: 'path',
                    required: true,
                    schema: {
                        type: 'integer'
                    }
                }],
                responses: new OpenApiResourceResponses('The deleted #RESOURCE_NAME# item')
            }
        },
    ];

    protected initializeResourceControllerBase(controller: ControllerBase, Model: ModelCtor<Model<any, any>>) {
        this.Model = Model;
        this.initializeControllerBase(controller);
    }

    protected checkResourceControllerBaseInitialized() {
        if (!this.Model) {
            throw new Error ('Resource controller has not been initialized. Call "initializeResourceControllerBase".');
        }
    }

    public getModel(): ModelCtor<Model<any, any>> {
        return this.Model;
    }
    
    protected async list(req: express.Request, res: express.Response) {
        this.checkResourceControllerBaseInitialized();
        const findOptions = this.parseListQueryParams(req);
        const count = await this.Model.count(findOptions.where ? {where: findOptions.where} : {});
        const items = await this.Model.findAll(findOptions);
        const query = {
            limit: findOptions.limit,
            offset: findOptions.offset,
            filter: req.query.filter,
            order: findOptions.order && Array.isArray(findOptions.order) && findOptions.order[0] ? findOptions.order[0] : undefined,
            attributes: findOptions.attributes,
        }
        return {items, count, query};
    }

    protected async get(req: express.Request, res: express.Response) {
        this.checkResourceControllerBaseInitialized();
        const model = await this.findModelById(req.params.id);
        return model;
    }

    protected async patch(req: express.Request, res: express.Response) {
        this.checkResourceControllerBaseInitialized();
        let model = await this.findModelById(req.params.id);
        this.updateModelWithBody(model, req.body);
        await model.validate();
        return await model.save();
    }

    protected async delete(req: express.Request, res: express.Response) {
        this.checkResourceControllerBaseInitialized();
        const model = await this.findModelById(req.params.id);
        return model.destroy();
        
    }

    private async findModelById(id: string): Promise<Model<any, any>> {
        const model = await this.Model.findByPk(id);
        if (!model) {
            throw new ErrorResponse (`Resource ${this.Model.name} with ID ${id} not found`, HttpStatus.NotFound);
        }
        return model;
    }

    private updateModelWithBody(model: Model<any, any>, body: any) {
        for (const key in body) {
            if (!this.Model.rawAttributes.hasOwnProperty(key)) {
                throw new ErrorResponse (`Attribute ${key} does not exist on resource ${this.Model.name}`, HttpStatus.UnprocessableEntity);
            }
            model.set(key, body[key]);
        }
        return model;
    }

    private parseListQueryParams(req: express.Request): FindOptions {
        // TODO: configba maxLimit, defaultLimit értékek
        const maxLimit = 50;
        const defaultLimit = 10;
        const findOptions: FindOptions = {
            limit: defaultLimit,
            offset: 0
        };
        let attributes: string[] = [];
        if (req.query.limit !== undefined) {
            if (Number.isInteger(Number(req.query.limit)) && Number(req.query.limit) > 0) {
                if (Number(req.query.limit) <= maxLimit) {
                    findOptions.limit = Number(req.query.limit);
                } else {
                    throw new ErrorResponse (`Query parameter limit cannot be larger than ${maxLimit}`, HttpStatus.BadRequest);
                }
            } else {
                throw new ErrorResponse (`Query parameter limit must be a positive integer`, HttpStatus.BadRequest);
            }
        }
        if (req.query.offset !== undefined) {
            if (Number.isInteger(Number(req.query.offset)) && Number(req.query.offset) > 0) {
                findOptions.offset = Number(req.query.offset);
            } else {
                throw new ErrorResponse (`Query parameter offset must be a positive integer`, HttpStatus.BadRequest);
            }
        }
        if (req.query.filter !== undefined) {
            if (typeof req.query.filter !== 'object' && req.query.filter === null) {
                throw new ErrorResponse (`Query param filter must be in format filter[{field}][{operator}]={value}`, HttpStatus.BadRequest);
            }
            const where = {} as any;
            for (const fieldName in req.query.filter as object) {
                if (!this.Model.rawAttributes.hasOwnProperty(fieldName)) {
                    throw new ErrorResponse (`Attribute ${fieldName} of query parameter filter does not exist on resource ${this.Model.name}`, HttpStatus.BadRequest);
                }
                const filter = req.query.filter as any;
                const field = filter[fieldName] as any;
                for (const operatorName in field) {
                    const enabledOperators = ['eq', 'ne', 'gt', 'gte', 'lt', 'lte', 'like', 'notLike'];
                    if (!enabledOperators.includes(operatorName)) {
                        throw new ErrorResponse (`Filter operator ${operatorName} is not a valid operator. It must be one of: ${enabledOperators.join(', ')}`, HttpStatus.BadRequest);
                    }
                    const value = field[operatorName];
                    if (operatorName === 'like' || operatorName === 'notLike') {
                        if (where[fieldName]) {
                            where[fieldName][Op[operatorName]] = `%${value}%`;
                        } else {
                            where[fieldName] = {[Op[operatorName]]: `%${value}%`};
                        }
                    } else if (operatorName === 'eq' || operatorName === 'ne' || operatorName === 'gt' || operatorName === 'gte' 
                    || operatorName === 'lt' || operatorName === 'lte'){
                        if (where[fieldName]) {
                            where[fieldName][Op[operatorName]] = value;
                        } else {
                            where[fieldName] = {[Op[operatorName]]: value};
                        }
                    }
                }
            }
            findOptions.where = where;
        }
        if (req.query.order !== undefined) {
            const orderArray = String(req.query.order).split(',');
            if (orderArray.length !== 2) {
                throw new ErrorResponse (`Query param order value must be in format {field},{direction}`, HttpStatus.BadRequest);
            }
            if (!this.Model.rawAttributes.hasOwnProperty(String(orderArray[0]))) {
                throw new ErrorResponse (`Attribute ${req.query.sort} of query parameter sortField does not exist on resource ${this.Model.name}`, HttpStatus.BadRequest);
            }
            if (orderArray[1] !== 'asc' && orderArray[1] !== 'desc') {
                throw new ErrorResponse (`Second part of value of query param order must be 'asc' or 'desc'`, HttpStatus.BadRequest);
            }
            findOptions.order = [[orderArray[0], orderArray[1]]];
        }
        if (req.query.attributes !== undefined) {
            const attributesArray = String(req.query.attributes).split(',');
            if (attributesArray.length < 1) {
                throw new ErrorResponse (`Query parameter attributes is empty. Format: {attribute1},{attribute2},...`, HttpStatus.BadRequest);
            }
            for (const attribute of attributesArray) {
                if (!this.Model.rawAttributes.hasOwnProperty(attribute)) {
                    throw new ErrorResponse (`Attribute ${attribute} of query parameter attributes does not exist on resource ${this.Model.name}`, HttpStatus.BadRequest);
                }
            }
            findOptions.attributes = attributesArray;
        }
        return findOptions;
    }
}