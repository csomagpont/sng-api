import Controller from "../interface/controller.interface";
import * as express from 'express';
import { HttpMethod } from "../../enum/http-method.enum";
import { DataResponse } from "../../response/data.response";
import { ErrorResponse } from "../../response/error.response";
import { Route } from "../interface/route.interface";
import { ValidationError, TimeoutError, DatabaseError, ConnectionError } from "sequelize";
import { HttpStatus } from "../../enum/http-status.enum";

type RequestHandlerFunction = (req: express.Request, res: express.Response) => Promise<any>;
type FinalRequestHandlerFunction = (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void>;

export abstract class ControllerBase implements Controller {
    public abstract path: string;
    public abstract routes: Route[] = [];
    public readonly router: express.Router = express.Router();
    protected controller!: ControllerBase;

    protected initializeControllerBase(controller: ControllerBase) {
        this.controller = controller;
        this.setupRoutes();
    }

    protected checkControllerBaseInitialized() {
        if (!this.controller) {
            throw new Error ('Base controller has not been initialized. Call "initializeControllerBase".');
        }
    }

    protected setupRoutes(): void {
        for (const route of this.routes) {
            this.request(route);
        }
    }

    private async request (route: Route) {
        const {path, method, handler, middleware} = route;
        this.checkControllerBaseInitialized();
        // TODO: logging, több middleware
        const finalHandler: FinalRequestHandlerFunction = async (req, res, next) => {
            try {
                const data = await handler.bind(this.controller)(req, res);
                res.send(new DataResponse(data));
            } catch (error) {
                this.handleEndpointError(error, res, next);
            }
        }
        if (middleware) {
            // TODO: több middleware
            this.router[method](`/${path}`, middleware, finalHandler);
        } else {
            this.router[method](`/${path}`, finalHandler);
        }
    }

    private handleEndpointError(error: Error, res: express.Response, next: express.NextFunction) {
        let errorResponse;
        if (error instanceof ErrorResponse) {
            errorResponse = error;
        } else if (error instanceof ValidationError) {
            errorResponse =  new ErrorResponse (error.message, HttpStatus.UnprocessableEntity);
        } else if (error instanceof TimeoutError) {
            errorResponse =  new ErrorResponse (error.message, HttpStatus.InternalError);
        } else if (error instanceof DatabaseError) {
            errorResponse =  new ErrorResponse (error.message, HttpStatus.UnprocessableEntity);
        } else if (error instanceof ConnectionError) {
            errorResponse =  new ErrorResponse (error.message, HttpStatus.InternalError);
        } else {
            next(error);
            return;
        }
        res.send(errorResponse);
    }

    // TODO: validator errorok, adatbázis hibakezelés
    // https://stackoverflow.com/questions/43647482/custom-validation-error-using-sequelize-js
}