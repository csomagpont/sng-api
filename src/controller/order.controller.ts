import { ResourceControllerBase } from "./base/resource-controller.base";
import { instantPromise } from "../util/promise";
import { sequelize } from "..";
import { Order } from "../model/order.model";
import { HttpMethod } from "../enum/http-method.enum";

export class OrderController extends ResourceControllerBase {

    public path = 'order';

    constructor() {
        super();
        this.initializeResourceControllerBase(this, Order);
    }
}