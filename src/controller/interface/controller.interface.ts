import { Router } from 'express';
import { Route } from './route.interface';

interface Controller {
  router: Router;
  path: string;
  routes: Route[];
}

export default Controller;