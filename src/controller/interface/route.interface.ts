import { HttpMethod } from "../../enum/http-method.enum";
import * as express from 'express';
import { HttpStatus } from "../../enum/http-status.enum";
import { OpenApiResponses, OpenApiParameter } from "../../openapi/openapi.interface";

type RequestHandlerFunction = (req: express.Request, res: express.Response) => Promise<any>;

export interface Route {
    path: string;
    method: HttpMethod;
    handler: RequestHandlerFunction;
    middleware?: express.RequestHandler;
    // TODO: Swagger adatok
    openapi: {
        summary: string,
        description?: string,
        parameters?: OpenApiParameter[],
        responses: OpenApiResponses
    }
}

